\LoadClass{article}[a4paper, 11pt]
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cv}[2024 Jonnobrow CV]

% Imports
\RequirePackage{fullpage} % Makes the CV fill the page more
\RequirePackage{graphicx}
\RequirePackage{fontawesome} % Icons Boi
\RequirePackage{hyperref} 
\RequirePackage[T1]{fontenc}
\RequirePackage{enumitem}
\RequirePackage{helvet}


% Colours
\RequirePackage[dvipsnames]{xcolor}
\definecolor{accent}{RGB}{57,61,71}

% Icons and Font
\renewcommand*{\faicon}[1]{{\makebox[1.5em][c]{\color{accent}\csname faicon@#1\endcsname}}}

% List spacing
\setlist{nosep}

% Change Headings to be pretty
\RequirePackage{titlesec}

\titleformat{\section} {\sffamily\Large\raggedright\scshape} {}{0em} {} [\titlerule]

\titleformat{\subsection} {\large\bfseries\raggedright} {}{0em} {}
\titlespacing{\subsection}{0pt}{*0}{*0}

\titleformat{\subsubsection} {\normalsize\raggedright} {}{0em} {}
\titlespacing{\subsubsection}{0pt}{*0}{*0}


\newcommand{\datedsubsubsection}[2]{%
  \subsubsection[#1]{\accentbold{#1} \hfill \textsc{#2}}%
}

% Letter section
\newcommand{\lettersection}[2]{
  \subsection[#1]{\textcolor{accent}{#1} #2}\titlerule
  \vspace{2mm}
}


% Split Lines for subsections
\newcommand{\splitline}[2]{
  \noindent\textcolor{accent}{#1} \hfill #2\newline
}

% Separator for the sections
\newcommand{\horizline}[1]{
  \begin{center}
  \rule{#1pt}{1pt}
  \end{center}
}

% Header Section
\newcommand{\headersection}[9]{
  \begin{center}
    {\sffamily\Huge\bfseries\color{accent} #1}
  \end{center}
  \faicon{mobile} #2 \hfill #3 \faicon{envelope} \newline
  \faicon{globe} #4 \hfill #5 \faicon{home} \newline
  \href{#7}{\faicon{github} #6} \hfill \href{#9}{#8 \faicon{linkedin}}\newline
}

% Bold
\let\oldtextbf\textbf
\newcommand{\accentbold}[1]{\oldtextbf{\color{accent}#1}}
\renewcommand{\textbf}[1]{\accentbold{#1}}

% Interests
\newcommand{\interest}[1]{\accentbold{#1}}

% Cover letter main body
\newenvironment{mainbody}{\begin{center}
  \begin{minipage}[adjusting]{0.95\linewidth}}{\end{minipage}\end{center}}
